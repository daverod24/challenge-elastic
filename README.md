# Challenge-elastic-vagrant

## Desafio plataforma elasticsearch con vagrant y ansible
Tutorial de uso de elasticsearch utilizando las herramientas de devops en un entorno virtualizado

## El stack formado por Ansible, docker y Vagrant.
Ante los desafíos de construir y mantener un software complejo, es realmente difícil administrar el aprovisionamiento, la orquestación, la construcción y el despliegue de aplicaciones fácilmente. Afortunadamente, hay herramientas y motores que pueden ayudarte.

En el siguiente tutorial, veremos cómo Ansible , Docker y Vagrant se pueden usar para aprovisionar e instalar el software necesario en el entorno en el que puede construir e implementar una aplicación. Una vez configurada, será posible ejecutar la aplicación en cualquier entorno donde estén instalados los requisitos previos.

Aquí hay un resumen rápido sobre las herramientas que vamos a utilizar:

### Ansible
Ansible es un motor de automatización de TI escrito en Python. Con Ansible es posible automatizar el aprovisionamiento, la orquestación, la gestión de la configuración y la implementación de aplicaciones.

Los Playbooks de Ansible se escriben utilizando la sintaxis YAML, de modo que lo tenga en formato legible por humanos y no se requiera un conocimiento complejo para comprender lo que hace. En la práctica, puede pasar sus Playbooks de Ansible a una tercera persona y en un par de minutos él/ella tendrá una idea de cómo administra el aprovisionamiento de su producto.

### Docker
Docker es un buen juguete para construir e implementar cualquier tipo de aplicación en contenedores ligeros de Linux. Es importante comprender que Docker no es una VM. A diferencia de las máquinas virtuales , Docker se basa en AUFS . Comparte el mismo núcleo y sistema de archivos de la máquina donde está alojado. Viene con una gran CLI que hace que la interacción con el motor Docker sea realmente fácil y admite el control de versiones de las imágenes.

### Vagrant
Vagrant es un administrador de máquinas virtuales. Es fácil de configurar y, por defecto, viene con soporte de proveedores como Docker, VirtualBox y VMware. Lo mejor de Vagrant es que puede usar todas las herramientas modernas de aprovisionamiento (por ejemplo, Chef, Puppet, Ansible) para instalar y configurar software en la máquina virtual.

### ELK



### La metas del desafio
- [x] hacer una imagen virtualbox para utilizarla como proveedor de Vagrant.
- [x] Ejecute Vagrant VM, aprovisione con Ansible todas las librerias necesarias para tener un entorno para probar ELK.
- [x] Creacion de usuario **"challenge"** con SUDO y Passwordless
- [x] Debe tener instalado java
- [x] Se deberá configurar como pre-requisitos de tunning de OS: limits, vm.max_map_count y asegurar las buenas practicas que requiera el servicio de Elasticsearch.
- [x] ANSIBLE deberá estar estructurado en Roles (utilizara un archivo SITE.YAML para orquestar la ejecución de los roles), aplicando las "best practices" indicadas en la documentación oficial de ANSIBLE, empleando variables, templates, handlers, etc... y los módulos que considere necesarios.
- [ ] Todos los discos extras para la implementación deben ser configurados con Thin provisioning.
- [ ] Deberá asignar un disco específico para docker storage al menos de 5GB.
- [ ] Deberá configurar ELASTICSEARCH utilizando un disco extra exclusivo para su data.
- [ ] Los componentes de logstash y kibana deberán externalizar su configuración en Volúmenes y al igual que ELASTICSEARCH. (Estos volúmenes deben ser entregados usando LVM)


### segunda fase del desafio
- [ ] Instalar maven y con el generar desde el portal de spring (https://start.spring.io/) un “hello world” de spring que usando el logback, este deberá hacer streaming de sus logs hacia la plataforma de ELASTIC STACK probando así la implementación realizada con ANSIBLE.
- [ ] Deberá compilar la aplicación, empaquetar y ejecutar el jar usando docker.
- [ ] ANSIBLE deberá mostrar un mensaje final con un output indicando el nombre del contenedor, ip de exposición, puerto de exposición y endpoint a consultar.
- [ ] Poder implementar Logs y metricas y validar que funciona

### Estructura de directorios

```shell
|-- README.md
|-- Vagrantfile
|-- inventory
|-- keys
|-- notas.md
|-- provision
|   |-- roles
|   |   |-- daverod24.common
|   |   |   |-- README.md
|   |   |   |-- defaults
|   |   |   |   `-- main.yml
|   |   |   |-- handlers
|   |   |   |   `-- main.yml
|   |   |   |-- meta
|   |   |   |   `-- main.yml
|   |   |   |-- tasks
|   |   |   |   |-- lvm.yml
|   |   |   |   |-- main.yml
|   |   |   |   `-- prerequisites.yml
|   |   |   |-- templates
|   |   |   |   `-- limits.conf.j2
|   |   |   `-- tests
|   |   |       |-- inventory
|   |   |       `-- test.yml
|   |   |-- daverod24.elasticsearch
|   |   |   |-- README.md
|   |   |   |-- defaults
|   |   |   |   `-- main.yml
|   |   |   |-- handlers
|   |   |   |   `-- main.yml
|   |   |   |-- meta
|   |   |   |   `-- main.yml
|   |   |   |-- tasks
|   |   |   |   |-- main.yml
|   |   |   |   |-- setup-Debian.yml
|   |   |   |   `-- setup-RedHat.yml
|   |   |   `-- templates
|   |   |       |-- elasticsearch.repo.j2
|   |   |       |-- elasticsearch.yml.j2
|   |   |       `-- elasticsearchold.yml.j2
|   |   |-- daverod24.java
|   |   |   |-- README.md
|   |   |   |-- defaults
|   |   |   |   `-- main.yml
|   |   |   |-- meta
|   |   |   |   `-- main.yml
|   |   |   |-- tasks
|   |   |   |   |-- main.yml
|   |   |   |   |-- setup-Debian.yml
|   |   |   |   `-- setup-RedHat.yml
|   |   |   |-- templates
|   |   |   |   `-- java_home.sh.j2
|   |   |   `-- vars
|   |   |       |-- Debian-10.yml
|   |   |       |-- Debian-9.yml
|   |   |       |-- RedHat-7.yml
|   |   |       |-- Ubuntu-16.yml
|   |   |       `-- Ubuntu-18.yml
|   |   |-- daverod24.kibana
|   |   |   |-- README.md
|   |   |   |-- defaults
|   |   |   |   `-- main.yml
|   |   |   |-- handlers
|   |   |   |   `-- main.yml
|   |   |   |-- meta
|   |   |   |   `-- main.yml
|   |   |   |-- tasks
|   |   |   |   |-- main.yml
|   |   |   |   |-- setup-Debian.yml
|   |   |   |   `-- setup-RedHat.yml
|   |   |   `-- templates
|   |   |       |-- kibana.repo.j2
|   |   |       `-- kibana.yml.j2
|   |   |-- daverod24.logstash
|   |   |   |-- README.md
|   |   |   |-- defaults
|   |   |   |   `-- main.yml
|   |   |   |-- files
|   |   |   |   `-- filters
|   |   |   |       |-- 10-syslog.conf
|   |   |   |       |-- 11-nginx.conf
|   |   |   |       `-- 12-apache.conf
|   |   |   |-- handlers
|   |   |   |   `-- main.yml
|   |   |   |-- meta
|   |   |   |   `-- main.yml
|   |   |   |-- tasks
|   |   |   |   |-- config.yml
|   |   |   |   |-- main.yml
|   |   |   |   |-- plugins.yml
|   |   |   |   |-- setup-Debian.yml
|   |   |   |   |-- setup-RedHat.yml
|   |   |   |   `-- ssl.yml
|   |   |   `-- templates
|   |   |       |-- 01-beats-input.conf.j2
|   |   |       |-- 02-local-syslog-input.conf.j2
|   |   |       |-- 30-elasticsearch-output.conf.j2
|   |   |       `-- logstash.repo.j2
|   |   `-- daverod24.usersudo
|   |       |-- README.md
|   |       |-- defaults
|   |       |   `-- main.yml
|   |       |-- meta
|   |       |   `-- main.yml
|   |       |-- tasks
|   |       |   |-- main.yml
|   |       |   |-- prerequisites.yml
|   |       |   `-- sudoers.yml
|   |       `-- tests
|   |           |-- inventory
|   |           `-- test.yml
|   |-- site.yml
|   `-- vars
|       `-- main.yml
|-- requirements.yml
`-- vagrant_docker
    `-- Dockerfile-ubuntu.dockerfile
```

## Paso 1 (requisito previo): Instalar Vagrant y virtualbox
Instalar Vagrant es fácil, mira la página de descarga y sigue las instrucciones.

Si el sistema operativo es Ubuntu/Debian:

```shell
sudo apt install virtualbox
sudo apt install vagrant

```


Si el sistema operativo es Centos:
```shell
#
cd /etc/yum.repos.d/

wget http://download.virtualbox.org/virtualbox/rpm/rhel/virtualbox.repo
yum update -y
yum -y install epel-release
yum -y install gcc make patch  dkms qt libgomp
yum -y install kernel-headers kernel-devel fontforge binutils glibc-headers glibc-devel
yum -y install VirtualBox-6.0

wget https://releases.hashicorp.com/vagrant/2.2.5/vagrant_2.2.5_x86_64.rpm

yum -y localinstall vagrant_2.2.5_x86_64.rpm

vagrant version
```

## Paso 2: escribir Vagrantfile
Las siguientes cosas están incluidas en el Vagrantfile

Cree una imagen de Docker y úsela como proveedor de VM
Reenvíe el 8080 al 7000 disponible para el host
Aprovisione la máquina con Ansible

```ruby
# -*- mode: ruby -*-
# vi: set ft=ruby :
#configure version values 1 or 2
$VAGRANT_CONFIGURE_VERSION= '2'


#vms array
#vms array
vms = {
  "challenge1-elastic"    => { :box => "bento/centos-7.6", :ip => "192.168.35.5", :cpus => 1, :mem => 1500 },
  # "challenge2-logstash"   => { :box => "bento/centos-7.6", :ip => "192.168.35.6", :cpus => 1, :mem => 1500 },
  # "challenge3-kibana"     => { :box => "bento/centos-7.6", :ip => "192.168.35.7", :cpus => 1, :mem => 1500 },
  # "challenge4-springboot" => { :box => "bento/centos-7.6", :ip => "192.168.35.8", :cpus => 1, :mem => 1500 },
}

Vagrant.configure(2) do |config|
    vms.each_with_index do |(virtualmachine, vminfo), index|
      config.vm.define virtualmachine do |cfg|
        cfg.vm.provider :virtualbox do |vb, override|
          config.vbguest.auto_update = false
          config.vm.box = vminfo[:box]
          override.vm.network :private_network, ip: vminfo[:ip]
          override.vm.hostname = virtualmachine
          vb.name = virtualmachine
          vb.customize ["modifyvm", :id, "--memory", vminfo[:mem], "--cpus", vminfo[:cpus], "--hwvirtex", "on"]
        end # end provider
        cfg.vm.synced_folder "/home/david/Documentos/pruebas de concepto/", "/home/vagrant/home"
      end # end config
      config.vm.hostname = "challenge1"
      config.vm.network "forwarded_port", guest: 8080, host: 7000, host_ip: "0.0.0.0", auto_correct: true

      config.vm.provision "shell", inline: "echo 'Empieza el aprovisionamiento espere unos minutos!'"
      config.vm.provision "ansible" do |ansible|
        ansible.galaxy_roles_path = "/vagrant/provision/roles"
        ansible.playbook = "provision/site.yml"
        ansible.verbose = "vv"
        ansible.galaxy_role_file = "requirements.yml"
        ansible.galaxy_command = "sudo ansible-galaxy install -r requirements.yml -p ./provision/roles"
        #ansible.inventory_path = "/vagrant/inventory"
        #ansible.galaxy_command = "sudo ansible-galaxy install --role-file=%{role_file} --roles-path=%{roles_path} --force"

      end # end provision ansible
    end # end vms
  end

```
## Paso 3: Correr vagrant con virtualbox

Para ejecutar si quieres arrancar tu entorno con docker o con virtualbox puedes  ejecutar los siguentes comandos

si quieres con virtualbox  solo ejecuta:

```shell
vagrant up

```
si deseas borrar todo el entorno ejecuta

```shell
vagrant destroy

```


## Paso 4: Crear un playbook de ansible
El playbook consta de un rol  


```yaml
---

-
  become: true
  remote_user: vagrant
  #connection: local
  gather_facts: true
  hosts: all
  vars_files:
    - vars/main.yml
  roles:
    - { role: daverod24.common, when: "ansible_os_family == 'RedHat'"}
    - { role: daverod24.usersudo, when: "ansible_os_family == 'RedHat'"}
    - { role: daverod24.java, java_packages: "java-1.8.0-openjdk", java_home: "/usr/bin/java",  when: "ansible_os_family == 'RedHat'"}
    - { role: daverod24.elasticsearch, elk_kibana_username: admin, elk_kibana_password: WowWhatAStrongPassword4,  when: "ansible_os_family == 'RedHat'"}
    # - { role: daverod24.logstash, when: "ansible_os_family == 'RedHat'"}
    # - { role:  daverod24.kibana, when: "ansible_os_family == 'RedHat'"}

```


Cree una ssh key y agreguela a el directorio keys

```shell
ssh-keygen -f keys/devops_rsa -t rsa -C "devops@testing"
```

¡Eso es! Abra la Terminal/Línea de comando y vaya al directorio raíz del proyecto y ejecute

```shell
vagrant up
```
Tomará un par de minutos antes de que Vagrant inicie e instale el software e implemente la aplicación. Una vez que Vagrant esté en funcionamiento, entre en la maquina creada en vagrant y ejecute:

```shell
vagrant ssh
```
Estaras dentro del entorno de prueba lo puedes comprobar por el siguiente prompt.

```shell
vagrant@challenge1:~$ # <---debe aparecer asi
```

Como puede ver, es realmente fácil reunir a Ansible, Docker y Vagrant y usar el poder de cada uno para tener una administración de configuración, aprovisionamiento, construcción y despliegue consistentes y fáciles de mantener.


### Comandos avanzados

Crear un storage de almacenamiento
